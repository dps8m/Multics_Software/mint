/*                                                      mmain.c    */
/*  MINT virtual machine, 32 bit ANSI-C version
    Originally written for MINT-2 by T. W. Griswold, Feb 4 1984
    Converted to MINT-3, ANSI-C and updated by M. D. Godfrey - 1989

    @Copyright 1990  T.W. Griswold and M.D. Godfrey                */
/*     Main module for MINT virtual machine

       Other modules required:

       mload.c      PF and PDUMP file routines
       mvm.c        the v_m itself: main loop and primitives
       mio.c        I/O routines
       mutil.c      routines for some longer primitives
		      and service routines
       proto.h      prototypes
       mdefs.h      global definitions
       mvars.h      global variables -- define/allocate
       mtext.h      global variables -- extern references          */

#include "syshdrs.h"
#include "mvars.h"

#ifndef MULTICS
void print_signon(void );
#endif
void print_signon()
{
  printf("mint usage: [options] [filename]\n");
  printf("options:\n");
  printf("   -v  - verbose mode\n");
  printf("   -f <filename>\n");
  printf("      <filename> is used as PF or PDUMP source\n");
  printf("   -i <string>\n");
#ifdef MULTICS
  printf("      <string> is read as input before reading from stdin.\n");
  printf("      <string> may be of form SI <filename> to read from file.\n");
#else
  printf(
     "      <string> is read as input before reading from stdin.\n"
     "      <string> may be of form SI <filename> to read from file.\n");
#endif
}

#ifdef MULTICS
int main(argc, argv)
int argc;
char *argv[];
#else
int main(int argc, char *argv[])
#endif
{
  FILE *fd;
  sint start, next; /*, arg1, arg2, arg3; */
  char fname[FNAMSIZ+2];
  /* char *machtype_ptr; */
  char *addfni;
  char *fpath;
  char stdinbuf[12];
#ifdef MULTICS
  char  hist_file[255];
  char *hist_ptr;
#endif

  verbose = 0;
  setbuf(stdout, NULL);
  setvbuf(stdin, stdinbuf, _IONBF, 1);
/*clang  start = 0; */
  start_string = FALSE;
  printf(
      "MINT-3 Virtual Machine (32-bit Virtual Memory): Version 1.2.4\n");
  fpath = getenv("MINT_HOME");
  if(fpath == NULL) {
      strcpy(fname, "");
  }
  else {
      strcpy(fname, fpath);
      strcat(fname, "/");
  }
  strcat(fname, "MCOMP.PDM");        /* pdload now reorders bytes. */
  if (argc > 1) {
    addfni = NULL;
/* The option code here is for future features. sys_load() determines
   type of load from file header (PD/PF)   */
    for (start = 1; start < argc; start++) {
/*       printf("argv: %s\n", &argv[start][0]); */
      if (argv[start][0] == '-') {
	for (next = 1; (argv[start][next] != '\0'); next++) {
   	  switch (argv[start][next]) {
	    case 'p': break;                /* obsolete option */
            case 'f': start++;
                      if(start == argc) {
                         printf("-f requires filename.\n");
                         exit(-1);
                      }
                      if(strlen(argv[start]) > FNAMSIZ) {
			printf("Argument is too long: %s\n", argv[start]);
                        exit(-1);
                      }
                      strcpy(fname, argv[start]);
                      addfni = argv[start];
                      next = strlen(addfni) - 1; break;
            case 'h':
            case '?': print_signon(); exit(-1); break;
            case 'i': start++;
                      if(start == argc) {
                         printf("-i requires filename.\n");
                         exit(-1);
                      }
                      if(strlen(argv[start]) > MAXARGLEN) {
			printf("Argument is too long: %s\n", argv[start]);
                        exit(-1);
                      }
                      strcpy(start_str_addr, argv[start]);
                      strcat(start_str_addr, "\n");
                      start_string = TRUE;
                      start_ptr = start_str_addr;
                      if(verbose)printf("-i %s", start_ptr);
                      next = strlen(argv[start]) - 1; break;
            case 'v': verbose = 1; break;
	    default:  printf("Unrecognized option: %c\n",
                             argv[start][next]);
	              print_signon(); exit(-1);
	    	      break;
	    }  /* switch */
         if(argv[start] == NULL) break;
         }     /* for  */
      }      /* if    */
    }    /* for (start = 1; start < argc; start++) */
  }    /* if (argc > 1)   */
  /* if(verbose)printf("start %d, argc %d\n", start, argc); */
  fd = fopen(fname, "rb");
  if (fd == NULL) {
      printf("\nError: Input file <%s> not found.\n", fname);
      exit(1);
  }
  vmemsize = VMSIZ;
  if ((pcb = (vmwrds *)malloc(vmemsize*sizeof(vmwrds))) == NULL) {
      printf("Allocation of VSTORE size %uK failed. Reducing size.\n",
         vmemsize/1024);
      vmemsize = vmemsize/2;
      while ((pcb = (vmwrds *)malloc(vmemsize*sizeof(vmwrds))) == NULL) {
         vmemsize = vmemsize/2; printf(".");
         if( vmemsize < 65536) {
            printf("Not enough VSTORE space.\n");
            exit(1);
         }
      }
  }
  printf("VSTORE size %uK words. ", vmemsize/1024);
  if(verbose)printf("\nLoading file: %s ", fname);
  load_sys(fd, vmemsize/1024);
  fclose(fd);
  fd = NULL;
#ifdef READLINE
  hist_ptr = getenv("HOME");
  strcpy(hist_file, hist_ptr);
  strcat(hist_file, "/.mint_hist");
  read_history(hist_file);
#endif
  v_m( );
  return(0);
}
