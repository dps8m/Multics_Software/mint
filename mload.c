/*                                                         mload.c  */
/*  MINT virtual machine, 32 bit ANSI-C version
    Originally written for MINT-2 by T. W. Griswold, Feb 4 1984
    Converted to MINT-3, ANSI-C and updated by M. D. Godfrey - 1989

    Updated 28 May 2003: pdload tests for endedness of the PDM
    file to avoid crash due to loading a wrong-ended file.

    Updated January 2004: pdmp and pdload changed to write and read
    variable size stacks. The version of PDUMP changed from **PD*
    to **PD2*.

    August 2004: Corrected use of long as VSTORE address to unsigned
    long.

    September 2006: Now use endedness test to reorder bytes of ints in PDM
    files as they are read in.  This removes the need for separate big end
    and little end PDM files.

    @Copyright 1990  T.W. Griswold and M.D. Godfrey     */

/*  MINT Portable Format and PDUMP Format I/O routines  */

#include "syshdrs.h"
#include "mtext.h"

#define PF       1
#define PDUMP    2
#define DFLOAD   3
#define FILERR   0
#define PHYSEND  1
#define LOGEND   2
#define DONTCARE 3
#define CNTRL_S  0x13
union
{
  sint l;
  char c[sizeof (sint)];
} u;

#ifdef MULTICS
static void reorder_b(wd)
int *wd;
#else
void reorder_b(int *wd)
#endif
{
  char tp[sizeof(sint)];
  unsigned int k;

  u.l = *wd;
  for(k=0; k < sizeof(sint); k++) {
    tp[k] = u.c[k];
  }
  for(k=0; k < sizeof(sint); k++) {
    u.c[k] = tp[sizeof(sint) - 1 - k];
  }
  *wd = u.l;
}

#ifdef MULTICS
void load_sys(fd, vmemsize) /* Note: ftype is set internally */
FILE *fd;
int vmemsize;
#else
void load_sys(FILE *fd, int vmemsize) /* Note: ftype is set internally */
#endif
{
  struct tm *tmptr;
  time_t   lt;
  sint     ftype, temp_yr;
  unsigned char fbuf[16];
  char     wrong_a;
  vmwrds  *cmem;
  uint     i;
  sint     k;
  char     tmp1[10];
  char     inbuf[8];
  char    *inbufp = inbuf;
  int      big_a, ld_version;

  u.l = 1;
  cmem = pcb;
  for ( i = 0 ; i < 65536 ; ++i) { *(cmem++) = 0; }  /* Clear low VSTORE */
  curr_lst_limit = STKSIZ;
  curr_ost_limit = STKSIZ;
  ftype = PF;
  rewind(fd);
#ifdef MULTICS
  fread(fbuf, 4, 4, fd);        /* read enough for endedness test. */
#else
  fread(&fbuf, 4, 4, fd);        /* read enough for endedness test. */
#endif
  rewind(fd);
  fgets(inbufp, 7, fd);
  fgetc(fd);                     /* position fd correctly */
  big_a = fbuf[11]+fbuf[12];     /* This tests the "upper" 16 bits of */
                                 /* the LP register */
  wrong_a = FALSE;
  if(((u.c[0] == 0) && (big_a != 0)) ||
     ((u.c[0] != 0) && (big_a == 0))){ wrong_a = TRUE; }
  /*  printf("inbuf: %s\n", inbufp); */
  if (strncmp(inbufp, "**PD", 4) == 0) {
      ftype = PDUMP;
      ld_version = 1;
      if (strncmp(inbufp, "**PD2*", 6) == 0) ld_version = 2;
  }
  else rewind(fd);
  switch(ftype) {
    case PF:
      if(verbose) printf("\n"); else printf(" ");
      printf("Start PF load");
      lst = malloc(sizeof(sint)*curr_lst_limit);     /* allocate stacks */
      ost = malloc(sizeof(sint)*curr_ost_limit);
      cmem = lst;
      for ( k = 0 ; k < curr_lst_limit ; ++k) { *(cmem++) = 0; } /* clear */
      cmem = ost;
      for ( k = 0 ; k < curr_ost_limit ; ++k) { *(cmem++) = 0; }

      if (pfload(fd) == PHYSEND)
        printf("\n ** Physical end of file before logical end **\n");
      printf(":\n");
      sp = ost; *sp = 9999;                     /* initialize stacks */
      lp = lst; *lp = 32768;
      break;
    case PDUMP:
      if(verbose && wrong_a){
	printf(
          "\nFile is PDUMP format, but appears to be for the wrong ");
        printf("byte-order architecture.\n");
        if (u.c[0] == 0) {
          printf ("The system you are using is BIGEndian.\n"); }
        else {
          printf ("The system you are using is LittleEndian.\n"); }
        printf("Byte order of integers will be reversed\n");
      }
      if(verbose) printf("\n"); else printf(" ");
      printf("Start PDUMP load");
      pdload(fd, ld_version, wrong_a);
      printf(":\n");
      break;
    default:
      inbuf[7] = 0;
      printf("Unknown file type with header: %s\n", inbuf);
      exit(-1);
      break;
  }
  pc  = pcb + 8;
  *pc = vmemsize;
  lt  = time(NULL);
  tmptr = localtime(&lt);
  pc  = pcb + 12;
  *pc++ = 6;
  temp_yr = tmptr->tm_year;
  while(temp_yr >= 100) temp_yr = temp_yr-100;
  sprintf(tmp1, "%02d%02d%02d", temp_yr, (tmptr->tm_mon)+1,
          tmptr->tm_mday);
  *pc++ = (tmp1[3] << 24) + (tmp1[2] << 16) + (tmp1[1] << 8) + tmp1[0];
  *pc   = (tmp1[5] << 8)  + tmp1[4];
}

#ifdef MULTICS
char accum(s, c)
uint *s;
char c;
#else
char accum(uint *s, char c)
#endif
{
  if (c >= '0' AND c <= '9') c -= '0';
  else if (c >= 'A' AND c <= 'F') c -= ('A' - 10);
  else {
    printf("\n c = %x : %c ", c,c); 
    return(FALSE);
  }
  *s *= 0x10;
  *s += c;
  return(TRUE);
}

#ifdef MULTICS
sint pfload(fp)
FILE * fp;
#else
sint pfload(FILE *fp)
#endif
{
  uint lnno = 1;
  uint sum, csum;
  sint c;

  pc = pcb;
  sum = csum = 0;
  while(TRUE) {
    c = fgetc(fp);
    if (c == EOF) return(PHYSEND);
    if ((c &= 0xff) == CNTRL_S) return(LOGEND);
    switch(c) {
      case CR:
      case LF:
        break;
      case COMMA:
#if 0
        csum ^= sum;
        if (sum & 0x80000000) /* sign extend */
          sum |= 0xf00000000;
        *pc++ = sum;
#else
        *pc++ = sum;
        csum ^= sum;
#endif
        sum = 0;
        break;
      case MINUS:
        csum ^= sum;
#ifdef MULTICS
        *pc++ = -(int)sum;
#else
        *pc++ = -sum;
#endif
        sum = 0;
        break;
      case SLASH:
        csum ^= sum;
        if (sum == 0) return(LOGEND);
        pc = pcb + sum;
        sum = 0;
        break;
      case PERIOD:
	if((lnno%50) == 0)printf(".");
        if (sum != csum) {
          printf("\n** checksum error at line %x: cksum %x sum %x **\n",
                 lnno, csum, sum);
          exit(1);
        }
        sum = 0;
        while((c = fgetc(fp)) != EOF) {
          if ((c &= 0xff) == CR OR c == LF OR c == 0x20) break;
          if (NOT accum( &sum,c)) {
            printf("\n** sum overflow at line %x **\n",lnno);
            exit(1);
          }
        }
        if (c == EOF) {
          printf("\n** unexpected EOF at line %x **\n",lnno);
          exit(1);
        }
        if (sum != lnno) {
          printf("\n** line number error, line %x, sum = %x **\n",
                 lnno, sum);
          exit(1);
        }
        sum = csum = 0;
        ++lnno;
        break;

      default:
        if (accum( &sum, c) == FALSE) {
          printf("\n** overflow in line %x **\n",lnno);
          exit(1);
        }
        break;
    }
  }
}

/*  Dump VSTORE in PDUMP format, primitive PDUMP  */

#ifdef MULTICS
void vdump(strpos)
uint strpos;
#else
void vdump(uint strpos)
#endif
{
  int   k;
  sint  t;
  FILE *fd;
  char  filnam[FNAMSIZ+1];

  if ((t = Cstr(strpos, filnam, FNAMSIZ) ) < 0 ) {
    printf("\n** Name too long: %s",filnam);
    dexit();
  }
  if (t == 0) {
    printf("\n** Illegal file name **");
    dexit();
  }
  k = 0;
  while (filnam[k] != 0) {
    if(isprint(filnam[k]) == 0) {
      printf("\n** Filename contains non printable character:\n");
      printf("%s\n", filnam);
      dexit();
    }
    k++;
  }
  while (( fd = fopen(filnam,"wb")) == NULL) {
    printf("\n** PDUMP: open error, file %s\n",filnam);
    printf("Type filename, or CR to quit: ");
    fgets(filnam, FNAMSIZ, stdin);
    if (strlen(filnam) == 0) exit(1);
  }
  pdmp(fd, filnam);
  fclose(fd);
  fd = NULL;
}

#ifdef MULTICS
void pdmp(fp, fname)
FILE *fp;
char *fname;
#else
void pdmp(FILE *fp, char *fname)
#endif
{
  uint i1, i2, temp, vw;

  printf("\nWriting PDUMP_v2  file %s", fname);
  rewind(fp);
  fprintf(fp, "**PD2*\n");
  temp = pc - pcb;
  fwrite(&temp, sizeof(sint), 1, fp);
  fwrite(&curr_lst_limit, sizeof(sint), 1, fp);
  fwrite(&curr_ost_limit, sizeof(sint), 1, fp);
  i1 = lp - lst;
  fwrite(&i1, sizeof(sint), 1, fp);
  i2 = sp - ost;
  fwrite(&i2, sizeof(sint), 1, fp);
  if(verbose) {
    printf(", lst start %u, ost start %u ", i1, i2);
  }
  if (ferror(fp)) goto wrterr;
  fwrite(lst, sizeof(sint), curr_lst_limit, fp);
  if (ferror(fp)) goto wrterr;
  fwrite(ost, sizeof(sint), curr_ost_limit, fp);
  if (ferror(fp)) goto wrterr;
  vw = put_vst(fp);
  printf(":\n");
  printf("%u data words written.\n", vw);
  return;

wrterr:
  printf("** Write error, file %s **\n",fname);
  exit(1);
}

#ifdef MULTICS
void pdload(fp, ld_version, wrong_a)
FILE *fp;
int ld_version;
char wrong_a;
#else
void pdload(FILE *fp, int ld_version, char wrong_a)
#endif
{
  sint lst_start, ost_start;
  sint vsize, temp;
  uint k;

  fread(&temp, sizeof(sint), 1, fp);
  if(wrong_a)reorder_b(&temp);
  pc = pcb + temp;
  if (ld_version == 1) {
    fread(&lst_start, sizeof(sint), 1, fp);
    fread(&ost_start, sizeof(sint), 1, fp);
    if(wrong_a) {
      reorder_b(&lst_start);
      reorder_b(&ost_start);
    }
    curr_ost_limit = STKSIZ;                /* size fixed at default */
    curr_lst_limit = STKSIZ;
  }
  else {
    fread(&curr_lst_limit, sizeof(sint), 1, fp);
    fread(&curr_ost_limit, sizeof(sint), 1, fp);
    fread(&lst_start, sizeof(sint), 1, fp);
    fread(&ost_start, sizeof(sint), 1, fp);
    if(wrong_a) {
      reorder_b(&curr_lst_limit);
      reorder_b(&curr_ost_limit);
      reorder_b(&lst_start);
      reorder_b(&ost_start);
    }
  }
  /* lst = malloc(20000);   // test of relocation */
  lst = malloc(sizeof(sint)*curr_lst_limit);    /* allocate stacks */
  ost = malloc(sizeof(sint)*curr_ost_limit);
  if (verbose) {
     printf(
#ifdef MULTICS
       "\nPD version %d. lst size %d, at %d, ost size %d, at %d\n",
       ld_version,  curr_lst_limit, c_rel(lst), curr_ost_limit,
       c_rel(ost));
#else
#ifdef x64_
       "\nPD version %d. lst size %d, at %ld, ost size %d, at %ld\n",
#else
       "\nPD version %d. lst size %d, at %d, ost size %d, at %d\n",
#endif
       ld_version,  curr_lst_limit, (intptr)lst, curr_ost_limit,
       (intptr)ost);
#endif
     printf("lst start %u, ost start %u\n", lst_start, ost_start);
  }
  lp = lst + lst_start;
  sp = ost + ost_start;
  if (feof(fp) OR ferror(fp)) goto rderr;

  fread(lst, sizeof(sint), curr_lst_limit, fp); /* load stacks */
  if (feof(fp) OR ferror(fp)) goto rderr;
  fread(ost, sizeof(sint), curr_ost_limit, fp);
  if (feof(fp) OR ferror(fp)) goto rderr;
  if(wrong_a) {
    for(k = 0; k< curr_lst_limit; k++) {
      reorder_b(&lst[k]);
    }
    for(k = 0; k< curr_ost_limit; k++) {
      reorder_b(&ost[k]);
    }
  }
  vsize = get_vst(fp, wrong_a);
  if(verbose) {
      printf("VSTORE size (words): %d. Top of allocated VSTORE: %d.",
             vmemsize>>2, vsize);
  }
  return;
rderr:
  if (feof(fp)) printf("** Unexpected EOF");
  else printf("** Read error");
  printf(" **\n");
  exit(1);
}

/* Pack zeros in MINT PDUMP file */

#ifdef MULTICS
sint put_vst(fp)
FILE *fp;
#else
sint put_vst(FILE *fp)
#endif
{
  sint  zero = 0;
  uint  endf, plim;
  uint  wcnt, zcnt, pcx;
  uint  word;

#ifdef MULTICS
  plim = c_rel (pcb + vmemsize); 
#else
  plim = (uint)(pcb + vmemsize);              /* generates gcc diag, but works */
#endif
  printf(" Size: %u, plim %u.", vmemsize, plim);
  pcx = 0;
  for ( pc = pcb, zcnt = 0, wcnt = 0, endf = FALSE ; pcx < plim ;) {
    word = *(pc++);
#ifdef MULTICS
    pcx = c_rel (pc);
#else
    pcx = (uint)pc;                           /* generates gcc diag, but works */
#endif
    if( pcx%10000 == 0) printf(".");
    if (word != 0) { fwrite(&word, sizeof(sint), 1, fp); ++wcnt; }
    else {
      ++zcnt;
      while ((word = *(pc++)) == 0 ) {
#ifdef MULTICS
        pcx = c_rel(pc);
#else
        pcx = (uint)pc;                       /* generates gcc diag, but works */
#endif
        if( pcx%100000 == 0) printf("-");
        ++zcnt;
        if (pcx >= plim) {
          endf = TRUE;
          printf("Z");
          break;
        }
      }
      if(endf){ fwrite(&zero, sizeof(sint), 1, fp);
        fwrite(&zero, sizeof(sint), 1, fp);}
      else {
      fwrite(&zero, sizeof(sint), 1, fp);
      fwrite(&zcnt, sizeof(sint), 1, fp);
      wcnt = wcnt + 2;
      fwrite(&word, sizeof(sint), 1, fp); ++wcnt; zcnt =  0;
      }
      if (endf) break;
    }
    if (endf) break;
  }
  if (ferror(fp)) {
    printf("\n** Write error **\n");
    exit(1);
  }
  return(wcnt);
}

#ifdef MULTICS
sint get_vst(fp, wrong_a)
FILE *fp;
char wrong_a;
#else
sint get_vst(FILE *fp, char wrong_a)
#endif
{
  sint zcnt, pcx;
  sint word;

  pcx = 0;
  for ( pc = pcb ; (NOT(feof(fp)) AND (pc < pcb + vmemsize) ) ; ) {
    if (pcx%10000 == 0) printf(".");
    fread(&word, sizeof(sint), 1, fp); zcnt = 1;
    if(wrong_a) reorder_b(&word);
    if (word != 0 ) { *pc = word; pc++; pcx++; }
    else {
      fread(&zcnt, sizeof(sint), 1, fp);
    if(wrong_a) reorder_b(&zcnt);
      if (zcnt == 0) break;
      pc = pc + zcnt;
    }
    if (zcnt == 0) break;
  }
  if (ferror(fp)) {
    printf("\n** Read error **\n");
    exit(1);
  }
  if (pc > pcb + vmemsize)
     printf("\n** Too many words read into VSTORE: %nu \n", pc);
  if (verbose)printf("Words loaded: %d. ", pcx);
  return((pc - pcb)>>2);
}
