/*                                                         proto.h   */
/*  Mint virtual machine, 32 bit ANSI-C version
    Originally written for MINT-2 by T. W. Griswold, Feb 4 1984
    Converted to MINT-3, ANSI-C and updated by M. D. Godfrey - 1989

    @Copyright 1990  T.W. Griswold and M.D. Godfrey                  */

/* Prototypes for all procedures.     */

#ifdef MULTICS
void load_sys(/*FILE *, vmwrds*/ );
void v_m(/*void*/);
sint inchar(/*sint*/);
sint dicmtch(/*uint, uint, sint*/);
void outchar(/*sint, sint*/);
sint matchs(/*uint, sint*/);
void vdump(/*uint*/);
sint opnf(/*uint, sint*/);
void closf(/*sint*/);
void dexit(/*void*/);
sint recovr(/*stkwrds **/);
void shostack(/*void*/);
char accum(/*uint *, char*/);
sint pfload(/*FILE **/);
void pdload(/*FILE *, int , char*/ );
sint Cstr(/*uint, char *, sint*/);
void pdmp(/*FILE *, char **/);
sint put_vst(/*FILE **/);
sint get_vst(/*FILE *, char */);
sint getseg(/*void*/);
sint getmchr(/*sint, sint*/);
void update_hist(/*void*/);
#else
void load_sys(FILE *, vmwrds );
void v_m(void);
sint inchar(sint);
sint dicmtch(uint, uint, sint);
void outchar(sint, sint);
sint matchs(uint, sint);
void vdump(uint);
sint opnf(uint, sint);
void closf(sint);
void dexit(void);
sint recovr(stkwrds *);
void shostack(void);
char accum(uint *, char);
sint pfload(FILE *);
void pdload(FILE *, int , char );
sint Cstr(uint, char *, sint);
void pdmp(FILE *, char *);
sint put_vst(FILE *);
sint get_vst(FILE *, char );
sint getseg(void);
sint getmchr(sint, sint);
void update_hist(void);
#endif
