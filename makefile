#ENV    = -O2 -Wall -DLinux
#TEST   = -ggdb
#CFLAGS = $(ENV) $(TEST)
#CC     = gcc
#OBJ    = o
#
#.SUFFIXES: .obj
#
#MODS = mmain.c mvm.c mload.c mio.c mutil.c proto.h mdefs.h mvars.h mtext.h
#OBJS = mmain.$(OBJ) mvm.$(OBJ) mload.$(OBJ) mio.$(OBJ) mutil.$(OBJ)
SRCS = mmain.c mvm.c mload.c mio.c mutil.c c_pl1.pl1
OBJS = mmain.cob mvm.cob mload.cob mio.cob mutil.cob c_pl1

#
## include -B/DE for debugging symbols
#
#mint: $(OBJS)
#	$(CC) -ggdb -o $@ $(OBJS) -lreadline -lcurses

CFLAGS = -def MULTICS

mint: $(OBJS)
	cc $(OBJS) -of mint

mmain.cob : mmain.c syshdrs.h mvars.h mdefs.h proto.h
	cc $(CFLAGS) mmain.c -spaf alm

mvm.cob : mvm.c syshdrs.h mtext.h mvars.h mdefs.h proto.h
	cc $(CFLAGS) mvm.c -spaf alm

mload.cob : mload.c syshdrs.h mtext.h mvars.h mdefs.h proto.h
	cc $(CFLAGS) mload.c -spaf alm

mio.cob : mio.c syshdrs.h mtext.h mvars.h mdefs.h proto.h
	cc $(CFLAGS) mio.c -spaf alm

mutil.cob : mutil.c syshdrs.h mtext.h mvars.h mdefs.h proto.h
	cc $(CFLAGS) mutil.c -spaf alm

c_pl1 : c_pl1.pl1
	pl1 c_pl1

#print: $(MODS)
#	enscript -A 2 -L 56 -2r -Pjanus2p -fCourier8 --header='%W|$$n|Page $$%' --margins=34:::40 $(MODS)
#
#HFILES = proto.h
## module contents
#mmain.$(OBJ): mmain.c $(HFILES) syshdrs.h mdefs.h mvars.h
#mvm.$(OBJ):   mvm.c   $(HFILES) syshdrs.h mdefs.h mtext.h
#mload.$(OBJ): mload.c $(HFILES) syshdrs.h mdefs.h mtext.h
#mutil.$(OBJ): mutil.c $(HFILES) syshdrs.h mdefs.h mtext.h
#mio.$(OBJ):   mio.c   $(HFILES) syshdrs.h mdefs.h mtext.h
##
#.c.$(OBJ):
#	$(CC) -c $(CFLAGS) $<
