/*                                                         mtext.h  */
/*  MINT virtual machine, 32 bit ANSI-C version
    Originally written for MINT-2 by T. W. Griswold, Feb 4 1984
    Converted to MINT-3, ANSI-C and updated by M. D. Godfrey - 1989

    @Copyright 1990  T.W. Griswold and M.D. Godfrey                 */

/*     Global data declarations */

#undef interrupt
#ifdef DEBUG_VM
extern uint trbuf[MAXTRB+2];
extern uint trinst[MAXTRB+2];
extern sint trsp[MAXTRB+2];
extern sint tropstk[MAXTRB+2][2];
extern sint trbptr;
#endif

extern sint verbose;
extern sint curr_ost_limit;        /* current operand stack length */
extern sint curr_lst_limit;        /* current link stack length */
extern stkwrds  *lst;              /*  link stack */
extern stkwrds  *ost;              /*  operand stack */

extern sint      vmdebgf;          /*  global debug mode flag */
extern sint      interrupt;
extern vmwrds    vmemsize;         /*  size of VSTORE in words */
extern stkwrds  *lp;               /*  link stack pointer  */
extern vmwrds   *pc;
extern vmwrds   *pcb;              /*  pointer to base of VSTORE */
extern vmwrds   *sp;               /*  operand stack pointer */

extern sint seglist[];             /*  I/O segments */
extern FILE *s_ptr[];
extern sint start_string;
extern char *start_ptr;
extern char start_str_addr[MAXARGLEN+3];
