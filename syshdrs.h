/*                                                      syshdrs.h   */
/*  MINT virtual machine, 32 bit ANSI-C version
    Originally written for MINT-2 by T. W. Griswold, Feb 4 1984
    Converted to MINT-3, ANSI-C and updated by M. D. Godfrey - 1989

    @Copyright 1990  T.W. Griswold and M.D. Godfrey               */

/* Common system headers for all vm-c source files */

#ifndef MULTICS
#include <stdint.h>
#endif
#include <stdio.h>
#ifndef MULTICS
#include <stdio_ext.h>
#include <stdlib.h>
#endif
#include <ctype.h>
#include <time.h>
#include <signal.h>
#ifndef MULTICS
#include <inttypes.h>
#endif
#ifdef OS2
#include <conio.h>
#endif
#ifndef MULTICS
#define READLINE
#endif
#ifdef  READLINE
#include <readline/readline.h>
#include <readline/history.h>
#endif

#ifdef MULTICS
#include <string.h>
#endif

#include "mdefs.h"
#include "proto.h"
#ifdef MULTICS
extern void * realloc ();
extern void * malloc ();
extern int c_rel ();
extern void * c_ptr ();
typedef long time_t;
extern long time ();
extern char * getenv ();
#endif
