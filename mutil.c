/*                                                       mutil.c    */
/*  MINT virtual machine, 32 bit ANSI-C version
    Originally written for MINT-2 by T. W. Griswold, Feb 4 1984
    Converted to MINT-3, ANSI-C and updated by M. D. Godfrey - 1989,
           July 12, 2004   - improved ESTOP display.
           Aug  16, 2004   - improved diagnostic output.

    @Copyright 1990  T.W. Griswold and M.D. Godfrey                 */

/*  MINT VM utilities  */

#include "syshdrs.h"
#include "mtext.h"
#ifdef MULTICS
void print_dump();
#else
void print_dump(FILE *);
#endif

void dexit() { shostack(); update_hist(); exit(3); }

void shostack()
{
  struct tm *timeptr;
  time_t     loctime;
  vmwrds    *pcd;
  FILE      *errout;
#ifdef MULTICS
  char       dstr[7];
  int        k;
#endif

  fprintf(stdout, "\nDump will be appended to mint-tr.trc.\n");
  fflush(stdout);
  pcd = pcb + 6;
  loctime = time(NULL);
  timeptr = localtime(&loctime);
#ifdef MULTICS
  for (k = 0; k < 6; k ++)
    dstr[k] = getmchr (5, k);
  dstr[6] = 0;
  fprintf(stdout, "======MINT System Date: %6s.", dstr);
#else
  fprintf(stdout, "======MINT System Date: %6s.", (char *)pcd);
#endif
  fprintf(stdout, " Time of dump: %s", asctime(timeptr));
  print_dump(stdout);
  if((errout = fopen("mint-tr.trc", "a")) == NULL) {
    printf("Dump file <mint-tr.trc> could not be opened for append.\n");
    return;
  }
#ifdef MULTICS
  fprintf(stdout, "======MINT System Date: %6s.", dstr);
#else
  fprintf(errout, "======MINT System Date: %6s.", (char *)pcd);
#endif
  fprintf(errout, " Time of dump: %s", asctime(timeptr));
  print_dump(errout);
  fprintf(errout, "\n");
  fclose(errout);
  errout = NULL;
}

#ifdef MULTICS
sint  getmchr(base, indx)
sint base;
sint indx;
#else
sint  getmchr(sint base, sint indx)
#endif
{
  sint cpos, k, chr;

  cpos = ((indx & 3) << 3);
  /* cpos = (3 - (indx & 3)) * 9; */ /* Multics fix for 9 bit bit endian */
  k = base + 1 + (indx >> 2);
  chr = (*(pcb + k) >> cpos) & 0xff;
  return(chr);
}

/*  Test string match (MATCH)

        zero-length key string always matches
        key string sinter than test string never matches     */

#ifdef MULTICS
sint  matchs(capadr, keyadr)
uint capadr;
sint keyadr;
#else
sint  matchs(uint capadr, sint keyadr)
#endif
{
  stkwrds *xsp;
  sint     stradr, strcomp, keycomp, charcomp, k;

  if ((keycomp = *(pcb + keyadr)) == 0) return(TRUE);
  stradr    = *(pcb + capadr++);
  charcomp  = *(pcb + capadr);
  strcomp   = *(pcb + stradr);
  if (keycomp > (strcomp - charcomp)) return(FALSE);
  for ( k = 0; k < keycomp; ++k ) {
    if( NOT(getmchr(keyadr, k) == getmchr(stradr, k + charcomp)))
       return(FALSE);
  }
  xsp = (pcb + capadr);
  *xsp = keycomp + charcomp;
  return(TRUE);
}

/*  Test a list of strings for match (DICMATCH) */

#ifdef MULTICS
sint  dicmtch(capadr, listnd, prvlnk)
uint capadr;
uint listnd;
sint prvlnk;
#else
sint  dicmtch(uint capadr, uint listnd, sint prvlnk)
#endif
{
  uint keyadr, link;

  link = *(pcb + prvlnk);
  while(TRUE) {
    keyadr = link + 1;
    if (matchs(capadr, keyadr)) return(prvlnk);
    if (link == listnd OR link == 0) return(prvlnk);
    prvlnk = link;
    link = *(pcb + link);
  }
}

/*  Convert MINT string to C string.  */

#ifdef MULTICS
sint  Cstr(mintptr, strptr, maxlen)
uint mintptr;
char *strptr;
sint maxlen;
#else
sint  Cstr(uint mintptr, char *strptr, sint maxlen)
#endif
{
  char   *sptr;
  sint    chrcnt, k;
  sint    overl = 1;

  sptr = strptr;
  if ((chrcnt = *(pcb + mintptr)) <= 0) return(0);
  if (chrcnt > maxlen) {
    overl = -1;
    chrcnt = maxlen;
  }
  for ( k = 0 ;; k++) {
    *sptr++ = getmchr(mintptr, k);
    if (--chrcnt == 0) break;
  }
  *sptr = '\0';
  return(overl);
}

#ifdef MULTICS
static  char *  opcode[81] = {"Illegal","GET","GETV","VAL","->","DUP","LOSE",
                      "GLKP","SLKP","+","-","*","/","NEG","FROM","MASK",
                      "UNION","DIFFER","COMPL","EQ","NE","LT","GT","LE",
                      "GE","NOT","AND","OR","XOR","CHOOSE","YES","NO",
                      "GO","DO","ENTRY","EXIT","GETCH","PUTCH","INCH",
                      "OPCH","MATCH","DICMATCH","STOP","PDUMP","TIME",
                      "OPENF","Illegal","CLOSEF","EXR","TRAP","<=>",
                      "TRUE","FALSE","ADV","ESTOP","ADIFF","->","<-",
                      "Illegal","EMULATE"};
#endif

#ifdef MULTICS
void print_dump(fname)
FILE *fname;
#else
void print_dump(FILE *fname)
#endif
{
  sint     i;
  uint    pcs, pc1, pc2;
  vmwrds   n;
  vmwrds  *pci;           /* local copy of pc.*/
  stkwrds *xsp, *xlp;
#ifndef MULTICS
  char *  opcode[81] = {"Illegal","GET","GETV","VAL","->","DUP","LOSE",
                      "GLKP","SLKP","+","-","*","/","NEG","FROM","MASK",
                      "UNION","DIFFER","COMPL","EQ","NE","LT","GT","LE",
                      "GE","NOT","AND","OR","XOR","CHOOSE","YES","NO",
                      "GO","DO","ENTRY","EXIT","GETCH","PUTCH","INCH",
                      "OPCH","MATCH","DICMATCH","STOP","PDUMP","TIME",
                      "OPENF","Illegal","CLOSEF","EXR","TRAP","<=>",
                      "TRUE","FALSE","ADV","ESTOP","ADIFF","->","<-",
                      "Illegal","EMULATE"};
#endif
/*clang   i   = 0;*/
  pci = pc;
#ifdef DEBUG_VM
  if ( vmdebgf ) {
    n = trbptr;
    if(n == MAXTRB) n = 1;
    fprintf(fname, "\nTrace buffer: First entry: %u, last entry: %u\n",
           n, trbptr-1);
    fprintf(fname,
       "Index  VSTORE     Instr  stk ptr   operand stack[sp, sp -1]\n");
    for ( i = n ; i < MAXTRB+1 ; i++ ) {
      if(i == (trbptr - 1))fprintf(fname, "->");
      else fprintf(fname, "  ");
      fprintf(fname, "%3d  %6u", i, trbuf[i]);
      if(trinst[i] < 81)fprintf(fname, "%10s ", opcode[trinst[i]]);
      else fprintf(fname, "    %6u ", trinst[i]);
      fprintf(fname, "   %3d %10d  ", trsp[i], tropstk[i][ 0]);
      if(trsp[i] > 0)fprintf(fname, "%10d", tropstk[i][ 1]);
      fprintf(fname, "\n");
      if(i == (trbptr - 1))fprintf(fname, "\n");
    }
    for ( i = 1 ; i < n ; i++ ) {
      if(i == (trbptr - 1))fprintf(fname, "->");
      else fprintf(fname, "  ");
      fprintf(fname, "%3d  %6u", i, trbuf[i]);
      if(trinst[i] < 81)fprintf(fname, "%10s ", opcode[trinst[i]]);
      else fprintf(fname, "    %6u ", trinst[i]);
      fprintf(fname, "   %3d %10d  ", trsp[i], tropstk[i][ 0]);
      if(trsp[i] > 0)fprintf(fname, "%10d", tropstk[i][ 1]);
      fprintf(fname, "\n");
      if(i == (trbptr - 1))fprintf(fname, "\n");
    }
  }
#endif
  pcs = (pci - pcb);
  n = vmemsize;
  if ((vmwrds *) pci >= (vmwrds *)(pcb + 40)) pci = (vmwrds *)(pci - 40);
  else pci = (vmwrds *)pcb;
#ifdef MULTICS
  if ((vmwrds *) pci >  (vmwrds *)(pcb + n )) pci = (vmwrds *) c_ptr (pci, n);
#else
  if ((vmwrds *) pci >  (vmwrds *)(pcb + n )) pci = (vmwrds *) (long) n;
#endif
  pc1 =  (vmwrds)(pci - pcb); pc2 =  (vmwrds)((pci - pcb) + 80);
  fprintf(fname,
          "\nVSTORE Dump: pc  = %u, dump %u -> %u\n", pcs, pc1, pc2);
  fprintf(fname,
          "VSTORE    numeric   opcode   numeric   opcode");
  fprintf(fname,
          "   numeric   opcode   numeric   opcode\n");
  fprintf(fname, "%6u:", (uint)(pci - pcb));
  for ( i = 0, n = 0 ; i < 80 ; ++i, ++n ) {
    if ( n >= 4) {
      fprintf(fname, "\n%6u:", (uint)(pci - pcb));
       n = 0; }
    fprintf(fname, " %9u", *pci);
    if((*pci > 0) && (*pci < 81))fprintf(fname, " %8s", opcode[*pci]);
    else fprintf(fname, " %8s", "        ");
    pci++;
  }
  fprintf(fname, "\n\nOP STACK:   sp =%4u\n", (uint)(sp - ost));
  for ( xsp = ost + (curr_ost_limit-1), i = curr_ost_limit-1 ; i > 9 ;
        --i, --xsp ) {
    if (*xsp) break; }
  i += (9 - i%10);
  fprintf(fname, "%3u:", (uint)(xsp - ost));
  for ( n = 0 ; i >= 0 ; --i, --xsp, ++n ) {
    if ( n >= 8) {
       fprintf(fname, "\n%3u:", (uint)(xsp - ost));
       n = 0; }
    if ((sp - ost) != (xsp - ost)) {
       fprintf(fname, " %10u", *xsp); }
       else {
       fprintf(fname, "  >%8u", *xsp); }
    if ( xsp == ost) break;
  }
  fprintf(fname, "\n\nLINK STACK: lp =%4u\n", (uint)(lp - lst));
  for ( xlp = lst + (curr_lst_limit-1), i = curr_lst_limit-1 ; i > 9 ;
        --i, --xlp ) {
    if (*xlp) break; }
  i += (9 - i%10);
  fprintf(fname, "%3u:", (uint)(xlp - lst));
  for ( n = 0 ; i >= 0 ; --i, --xlp, ++n ) {
    if ( n >= 8) {
        fprintf(fname, "\n%3u:", (uint)(xlp - lst)); n = 0; }
    if((lp - lst) != (xlp - lst)) {
      fprintf(fname, " %10u",*xlp); }
    else {
      fprintf(fname, "  >%8u",*xlp); }
    if( xlp == lst) break;
  }
  fprintf(fname, "\n");
}

