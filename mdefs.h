/*                                                      mdefs.h */
/*  MINT virtual machine, 32 bit ANSI-C version
    Originally written for MINT-2 by T. W. Griswold, Feb 4 1984
    Converted to MINT-3, ANSI-C and updated by M. D. Godfrey - 1989

    @Copyright 1990  T.W. Griswold and M.D. Godfrey       */

/*              Configuration Parameters */

/* These defines are required for x64 systems. They shoud have no effect for i386. */
/* This is a start at conversion to x64. It has the effect that MINT runs on */
/* x64 architecture, but in 32bit VSTORE, and stack units are still 32bits. */
/* Of course, pointers to objects are 64 bits.  When pointers are stored on the */
/* stack, as for SLKP/GLKP, or in VSTORE, truncation is to 32 bits. */
/* gcc on an x64 system warns about mismatch. */

/* VM-C expects the following object sizes: */
/* */
/*          x86          x64 */
/* int      32           32 */
/* vstore   32           32 */
/* stacks   32           32 */
/* */
/* The VM uses sint for (signed) int and uint for unsigned int. */
/* The VM also uses vmwrds for VSTORE objects and stkwrds for stack objects. */
/* This is done to facilitate 64 bit expansion. At present these are */
/* both type sint. */
/* This will work since all VSTORE addresses that are stored in VSTORE */
/* or the stacks are VSTORE-base relative.  Therefore, they only need to */
/* be 32 bits in size. */
/* This restricts the maximum size of VSTORE to 4GB. On Linux Intel architecture */
/* systems, including AMD x64 as "Intel," this actually means that VSTORE */
/* can be 3GB on x386, and 4GB on x64. */
 
#if __WORDSIZE == 64
  #define x64_ 
#endif
/* sint and uint may be redefined, and vmwrds and stkwrds may also be changed */
#define sint       int
#define uint       unsigned int
#define vmwrds     sint
#define stkwrds    sint
#define intptr     intptr_t      /* 32bit on IA32, 64bit on x64 */

#ifdef MULTICS
#define MASK32  0xFFFFFFFF
#define SIGN32  0x80000000
#define SIGNEXT 0xF00000000
#endif
/* These are MINT VM Parameters */
#define DEBUG_VM
#define MAXTRB     101           /* Length of Trace buffer */

#define FNAMSIZ    255           /* Max size for a file name */
#define MAXARGLEN  255           /* Max string length as argument on mint command */

#define NSEGS      256           /* Segs are I/O channels */

#define STKSIZ     100
#define OST_INCR    50           /* op stack expansion. */
#define VMSIZ      4096*4096     /* in MINT Vstore words: 16M words */

/* Note that the layout of Item Space and the Record Pool space in VSTORE is */
/* declared at the beginning of MINTSUBS. These areas are bounded by */
/* ILOC to MAXILOC and */
/* RLOC to MAXRLOC. */
/* The VM initializes MAXDS$ amd MAXPS$ */
/* in low VSTORE (defined below), and the compiler intializes MAXDLOC and MAXPLOC */
/* from the values in MAXDS$ and MAXPS$. */

/*        Definitions for MINT Loader and Utilities */

#define TRUE       1
#define FALSE      0
#define NOT        !
#define OR         ||
#define AND        &&

#define BKSPC      0x8
#define LF         0xa
#define FF         0xc
#define CR         0xd
#define DC3        0x13
#define CTLZ       0x1a
#define BLANK      ' '
#define COMMA      ','
#define MINUS      '-'
#define SLASH      '/'
#define PERIOD     '.'

/*  segment types  */

#define UNUSED     0
#define SEQINP     1
#define SEQOUT     2
#define RANDOM     3
#define MAGTAP     4
#define COMMIO     5
#define MAXDS$     1    /*  # 128-word pages in data space */
#define MAXPS$     2    /*  ditto procedure space */
#define CONT$      3    /*  addr of abnormal status routine */
#define IDLOC$     4    /*  initial DLOC value */
#define SYSDAT$    5    /*  system generation date */
#define EXOPT$     9    /*  execution options */
#define DREM      11    /*  remainder from integer divide */
#define DATE      12    /*  current date */
#define SYSID$    16    /*  length of ID in chars */
