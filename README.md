```
The MINT system is a set of tools to facilitate communication with,
and operation of, computers. These tools provide a high level
software environment which is machine-independent and open-ended.

The machine-independence implies that the MINT system, and MINT
based applications, are readily portable to many machines. The open-
endedness implies considerable flexibility in altering or extending
the language facilities. The language itself allows sequences and
expressions at as high or as low a level as is desired.

MINT is implemented in terms of a Virtual Machine which allows
exactly the same (virtual) environment to exist regardless of the
actual machine on which the system is operating. This Virtual Machine
is referred to as the VM(M) Virtual Machine, and the instructions which
the Virtual Processor executes are the VM(M) instruction set.

Careful definition of this Virtual Machine contributes to the portability,
compactness, efficiency, and verifiable correctness of the system.

------

This is a port of the MINT virtual machine to Multics.

The loading of "portable format" object files is not working; that
format does not reliably mark signed data, so some negative integers
in the file are being interpreted as large unsiged integers. This
is a result of the 32 bit representation of data in the portable
format and the 36 bit port.

MOTE: It may be feasible to just sign extend the unsigned data; the
correctness would depend on the specific object file contents. Do
this first on the diagnostics and see if they pass (fixing the vm as
needed). If that works, then sign extend the compiler and self-compile.

https://sites.google.com/site/michaeldgodfrey/mint
```
