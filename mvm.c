/*                                                     mvm.c        */
/*  MINT virtual machine, 32 bit ANSI-C version
    Originally written for MINT-2 by T. W. Griswold, Feb 4 1984
    Converted to MINT-3, ANSI-C and updated by M. D. Godfrey - 1989
    @Copyright 2002 M.D. Godfrey

    16 Feb. 2002: Corrected TRAP  mdg
    NDP-C is smart enough to compile optimized expressions. Therefore,
    for use with NDP-C the tag NDPC make be defined.
    Most other compilers, including cc on Sparc and gcc do not compile
    the optimized expressions "correctly."

    January 2004: Changed to use malloc to obtain space for link and
    operand stacks. Used realloc to implement operand stack expansion.
    Changed pddump to provide stack sizes so that pdload can load a
    file without assuming the stack sizes.

    August 2004: Corrected use of long as VSTORE address to unsigned
    long.
    */

#include "syshdrs.h"
#include "mtext.h"  /*  extern; allocation is done in mvars.h  */

                   /*  Primitives  */

#define ILL_0      0
#define GET        1
#define GETV       2
#define VAL        3
#define PUTV       4    /*  ->  */
#define DUP        5
#define LOSE       6
#define GLKP       7
#define SLKP       8
#define ADD_       9
#define SUB_      10
#define MULT_     11
#define DIV_      12
#define NEG       13
#define FROM      14
#define MASK      15    /*  bitwise AND  */
#define UNION_    16    /*  bitwise OR  */
#define DIFFER    17    /*  bitwise XOR  */
#define COMPL     18    /*  bit complement  */
#define EQ_       19
#define NE_       20
#define LT_       21
#define GT_       22
#define LE_       23
#define GE_       24
#define NOT_      25
#define AND_      26
#define OR_       27
#define XOR_      28
#define CHOOSE    29
#define YES       30
#define NO        31
#define GO_       32
#define DO_       33
#define ENTRY     34
#define EXIT      35
#define GETCH     36
#define PUTCH     37
#define INCH      38
#define OPCH      39
#define MATCH     40
#define DICMATCH  41
#define STOP      42
#define PDUMP     43
#define TIME      44
#define OPENF     45
#define ILL_46    46
#define CLOSEF    47
#define EXR       48
#define TRAP      49
#define EXCH      50
#define TRU       51
#define FALS      52
#define ADV       53
#define ESTOP     54
#define ADIFF     55
#define SHIFTR    56
#define SHIFTL    57
#define DEBUGF    58
#define ILL_59    59
#define ILL_60    60
#define ILL_61    61
#define ILL_62    62
#define ILL_63    63
#define ILL_64    64
#define ILL_65    65
#define ILL_66    66
#define ILL_67    67
#define ILL_68    68
#define ILL_69    69
#define ILL_70    70
#define ILL_71    71
#define ILL_72    72
#define ILL_73    73
#define ILL_74    74
#define ILL_75    75
#define ILL_76    76
#define ILL_77    77
#define ILL_78    78
#define ILL_79    79
#define EMULATE   80

#define ENABLE_TRAPS
#ifdef OS2
#define sig_return _SigFunc
#else
#ifdef MULTICS
#define sig_return int
#else
#define sig_return void *
#endif
#endif

/*  trap parameters  */

sint  tractive  = 0;
sint  tron      = 0;

#ifdef MULTICS
sig_return catchint(sigin)
int sigin;
#else
sig_return catchint(int sigin)
#endif
{
  if(interrupt == 1) exit(-4);
  interrupt = 1;
  if(sigin == SIGINT) {
     printf("\nVM Interrupted:\n ");
     signal(SIGINT, (sig_return) catchint);
  }
  if(sigin == SIGSEGV) {
     printf("\nVM Address exception:\n");
     shostack();
     signal(SIGSEGV, (sig_return) catchint);
  }
  return 0;
}

/*  The Virtual Machine  */

void v_m( )
{
  vmwrds    *pcc;
  stkwrds   *spp;              /* Local declarations. */
                               /* There was a time when this improved */
                               /* efficiency. May not be needed these days. */
  uint      instruct;
  sint      debugst;
  vmwrds    tmp1, tmp2;
  uint      tmp3, tmp4;
  vmwrds   *tmpz;
  time_t    crtime;
  sint      inst_cnt = 0;
  char      sysstr[255];
  vmwrds   *cmem;
  stkwrds   prev_ost_limit;
  stkwrds   prev_spp;
  stkwrds   ost_incr = OST_INCR;

/*  Clear the I/O segment vaues = no files open  */

  for ( tmp1 = 0 ; tmp1 <= NSEGS ; ++tmp1 ) {
    seglist[tmp1] = 0;
    s_ptr[tmp1] = NULL;
  }

  signal(SIGINT, (sig_return) catchint);
  signal(SIGSEGV, (sig_return) catchint);
  debugst = 0;
  vmdebgf = 0;          /* debug initially off */

  pcc = pcb + (*lp)++;
  spp = sp;
  tractive = 0;
  tron = 0;
  if(verbose){
#if defined(x64_)
      printf("Initial pcb %lu, pcc %lu, pcc-pcb %d, spp %ld, lp %ld\n",
#else
      printf("Initial pcb %u, pcc %u, pcc-pcb %d, spp %d, lp %d\n",
#endif
#ifdef MULTICS
	     c_rel(pcb), c_rel(pcc), c_rel(pcc) - c_rel(pcb), c_rel(spp), c_rel(lp));
#else
	     (intptr)pcb, (intptr)pcc, (int)(pcc - pcb), (intptr)spp, (intptr)lp);
#endif
      fflush(stdout);}

/*  Virtual machine main loop */
  for( ;; ) {

/*  Trap test:  if tractive set, load the trap-procedure address into the
    pc, pass the address of the next normal instruction to the trap
    procedure (so that it can reference it,  and turn off the trap flag (so
    that the trap processing procedure isn't trapped).
*/
#ifdef ENABLE_TRAPS
    if (tractive) {
      tractive = 0;           /*  don't trap inside the trap procedure */
      *(++spp) = pcc - pcb;
      *lp = tron;             /*  pass address of next normal instr */
      pcc = pcb + *lp;
    }

/*  enter here to execute the next instruction unconditionally */

extrap:
#endif
#ifdef DEBUG_VM
      if (debugst){
      if ( trbptr > MAXTRB ) trbptr = 1;
      trinst[trbptr] = *pcc;
      tropstk[trbptr][ 0] = *spp;
      trsp[trbptr] = (spp - sp);
      if(spp == sp) tropstk[trbptr][ 1] = -9999;
      else tropstk[trbptr][ 1] = *(spp - 1);
      trbuf[trbptr++] = pcc - pcb;
      if ((pcc < pcb + 80) || (pcc > pcb + 65536)) {
	fprintf(stdout,"\nVM Bad vstore addr of %u\n",
               (uint) (pcc - pcb - 1));
        pc = pcc;
	if(recovr(spp)) {pcc = pcb + *lp; } else exit(3);}
      }
#endif
      if (spp < ost) {
         fprintf(stdout,
                 "\nOperand stack underflow at %d. Stack pointer: %d\n",
                 (sint)(pcc - pcb), (sint)(spp - ost));
         printf("curr_ost_limit %d\n", curr_ost_limit);
         shostack(); exit(-1);
      }
      if (spp >= ost + curr_ost_limit - 1) {
         if (spp < ost + curr_ost_limit - 1 + ost_incr) {
           prev_spp = spp - ost;
           prev_ost_limit = curr_ost_limit;
           curr_ost_limit = curr_ost_limit + ost_incr;
           if((ost = realloc(ost, sizeof(sint)*curr_ost_limit)) == NULL){
              fprintf(stdout,
                 "Attempt to expand Operand Stack failed at size %d\n",
                 curr_ost_limit);
              exit(-1);
           }
           spp = ost + prev_spp;
           cmem = ost + prev_ost_limit;
           for (tmp2 = prev_ost_limit; tmp2 < curr_ost_limit;
                tmp2++) *(cmem++) = 0;
           if (verbose) {
              fprintf(stdout, "Operand stack size incremented by %d\n",
                      ost_incr);
	      fflush(stdout);
           }
         }
         else {
           fprintf(stdout,
             "\nOperand-stack bounds failure at %d. Stack pointer: %d\n",
             (sint)(pcc - pcb), (sint)(spp - ost));
	   shostack(); exit(-1);
         }
      }
      if((lp < lst) || (lp >= lst + curr_lst_limit - 1)) {
         fprintf(stdout,
                "Link stack bounds failure at %d. Stack pointer: %d\n",
                (sint)(pcc - pcb), (sint)(lp - lst));
         pc = pcc;
         shostack(); exit(-1); }
      instruct = *(pcc++);
      pc = pcc;                  /* only required for error handling */
                                 /* pc is global, pcc is local. */
#ifdef DEBUG_VM
      inst_cnt++;
#endif
      switch(instruct) {

      case GET:   *(++spp) = *(pcc++); break;
      case GETV:  *(++spp) = *(pcb + *(pcc++)); break;
      case VAL:   *spp = *(pcb + *spp); break;
#ifdef NDPC
      case PUTV:  *(pcb + *(spp--)) = *(spp--); break;
#else
      case PUTV:  tmpz  = (pcb + *(spp--));
                  *tmpz = *(spp--); break;
#endif
      case DUP:   tmp1 = *(spp++);
                  *spp = tmp1; break;
      case LOSE:  --spp; break;
#ifdef MULTICS
      case GLKP:  *(++spp) = c_rel(lp); break;
#else
      case GLKP:  *(++spp) = (vmwrds)lp; break;     /* generates gcc diag, but works */
#endif
      case SLKP:  *lp  = pcc - pcb;
                  tmp3 = *lp;
#ifdef MULTICS
                  lp   = c_ptr (lp, *spp); spp--;
#else
                  lp   = (int *)*(spp--);         /* generates gcc diag, but works */
#endif
                  *lp  = tmp3;
                  pcc  = pcb + tmp3; break;
#ifdef NDPC
      case ADD_:  *spp += *(spp--); break;
      case SUB_:  *spp -= *(spp--); break;
      case MULT_: *spp *= *(spp--); break;
#else
#ifdef MULTICS
      case ADD_:  tmp1 = *(spp--); *spp += tmp1; (*spp) &= MASK32; break;
      case SUB_:  tmp1 = *(spp--); *spp -= tmp1; (*spp) &= MASK32; break;
      case MULT_: tmp1 = *(spp--); *spp *= tmp1; (*spp) &= MASK32; break;
#else
      case ADD_:  tmp1 = *(spp--); *spp += tmp1; break;
      case SUB_:  tmp1 = *(spp--); *spp -= tmp1; break;
      case MULT_: tmp1 = *(spp--); *spp *= tmp1; break;
#endif
#endif
#ifdef MULTICS
      case DIV_:
	  tmp1 = *(spp--);
            if (tmp1 & SIGN32)
              tmp1 |= SIGNEXT;
	  tmp2 = *spp;
            if (tmp2 & SIGN32)
              tmp2 |= SIGNEXT;
	  *(pcb + DREM) = (tmp2%tmp1) & MASK32;
	  *spp = (tmp2/tmp1) & MASK32; break;
      case NEG:   *spp = (-*spp) & MASK32; break;
#else
      case DIV_:
	  tmp1 = *(spp--);
	  tmp2 = *spp;
	  *(pcb + DREM) = tmp2%tmp1;
	  *spp = tmp2/tmp1; break;
      case NEG:   *spp = -*spp; break;
#endif
#ifdef NDPC
      case FROM:   *spp += *(spp--); break;
      case MASK:   *spp &= *(spp--); break;
      case UNION_: *spp |= *(spp--); break;
      case DIFFER: *spp ^= *(spp--); break;
#else
#ifdef MULTICS
      case FROM:   tmp1 = *(spp--); *spp += tmp1; (*spp) &= MASK32; break;
#else
      case FROM:   tmp1 = *(spp--); *spp += tmp1; break;
#endif
      case MASK:   tmp1 = *(spp--); *spp &= tmp1; break;
      case UNION_: tmp1 = *(spp--); *spp |= tmp1; break;
      case DIFFER: tmp1 = *(spp--); *spp ^= tmp1; break;
#endif
#ifdef MULTICS
      case COMPL: *spp = (~*spp) & MASK32; break;
#else
      case COMPL: *spp = ~*spp; break;
#endif
      case EQ_:   tmp1 = *(spp--); *spp = *spp == tmp1; break;
      case NE_:   tmp1 = *(spp--); *spp = *spp != tmp1; break;
#ifdef MULTICS
      case LT_:   tmp1 = *(spp--); 
                  if (tmp1 & SIGN32)
                    tmp1 |= SIGNEXT;
                  tmp2 = *spp;
                  if (tmp2 & SIGN32)
                    tmp2 |= SIGNEXT;
                  *spp = tmp2 <  tmp1; break;
      case GT_:   tmp1 = *(spp--); 
                  if (tmp1 & SIGN32)
                    tmp1 |= SIGNEXT;
                  tmp2 = *spp;
                  if (tmp2 & SIGN32)
                    tmp2 |= SIGNEXT;
                  *spp = tmp2 >  tmp1; break;
      case LE_:   tmp1 = *(spp--); 
                  if (tmp1 & SIGN32)
                    tmp1 |= SIGNEXT;
                  tmp2 = *spp;
                  if (tmp2 & SIGN32)
                    tmp2 |= SIGNEXT;
                  *spp = tmp2 <= tmp1; break;
      case GE_:   tmp1 = *(spp--); 
                  if (tmp1 & SIGN32)
                    tmp1 |= SIGNEXT;
                  tmp2 = *spp;
                  if (tmp2 & SIGN32)
                    tmp2 |= SIGNEXT;
                  *spp = tmp2 >= tmp1; break;
#else
      case LT_:   tmp1 = *(spp--); *spp = *spp <  tmp1; break;
      case GT_:   tmp1 = *(spp--); *spp = *spp >  tmp1; break;
      case LE_:   tmp1 = *(spp--); *spp = *spp <= tmp1; break;
      case GE_:   tmp1 = *(spp--); *spp = *spp >= tmp1; break;
#endif
#ifdef MULTICS
      case NOT_:  *spp = (NOT *spp) & MASK32; break;
#else
      case NOT_:  *spp = NOT *spp; break;
#endif
      case AND_:  tmp1 = *(spp--); *spp = tmp1 AND *spp; break;
      case OR_:   tmp1 = *(spp--); *spp = tmp1 OR  *spp; break;
      case XOR_:  tmp1 = *(spp--); *spp = tmp1 ^   *spp; break;
      case CHOOSE:
	  tmp1 = *(spp--);
	  tmp2 = *(spp--);
	  *spp = *spp ? tmp2 : tmp1; break;
      case YES:
          tmp1 = *(spp--);
          if (*spp--) {
             *lp = tmp1; pcc = pcb + *lp;
          } break;
      case NO:
          tmp1 = *(spp--);
          if (NOT *spp--) {
             *lp = tmp1;
             pcc = pcb + *lp;
          } break;
      case GO_:
          pcc = pcb + *spp--;
          *lp = pcc - pcb; break;
      case DO_:
          *(lp++) = pcc - pcb;
          *lp = *spp--;
          pcc = pcb + *lp; break;
      case ENTRY: break;
      case EXIT:
          --lp;
          pcc = pcb + *lp;
          *lp = pcc - pcb; break;
      case GETCH:
	/* *spp = get_ch(*spp); */
	  break;
      case PUTCH:
	/*  tmp1 = *spp-- ; */
	/*  put_ch(tmp1, *spp--); */
	  break;
      case INCH:  *spp = inchar(*spp); break;
      case OPCH:
	  tmp1 = *spp-- & 0xff;
	  outchar(tmp1, *spp--);
	  break;
      case MATCH:
	  tmp3 = *spp--;                     /*  cap             */
	  *spp = matchs(tmp3, *spp);         /*  *spp = @kstring */
	  break;
      case DICMATCH:
	  tmp3 = *spp--;                     /*  cap             */
	  tmp4 = *spp--;                     /*  @listend        */
	  *spp = dicmtch(tmp3, tmp4, *spp);  /*  *spp = @listp   */
	  break;
      case STOP:
	  fprintf(stdout,"\nVM STOP at %u\n",
                 (uint)(pcc - pcb - 1));
          update_hist();
	  exit(0);
      case PDUMP:
          pc = pcc;
          sp = spp;
          *lp = pcc - pcb;
          tmp3 = *sp--;
          vdump(tmp3); break;
      case TIME:
          time(&crtime);
          *(++spp) = 0xffff & crtime; break;
      case OPENF:
          tmp3 = *spp--;
          *spp = opnf(tmp3, *spp); break;
      case CLOSEF: closf(*spp--); break;
      case EXR:
          Cstr(*spp--, sysstr, 255);
          system(sysstr);
          break;
      case TRAP:
#ifdef ENABLE_TRAPS
          if((tractive && tron) || ((tractive == 0) && (tron == 0))) {
            tron     = *spp;
            tractive = *spp--;
            if(tron != 0) {
               lp--;  pcc = pcb + *lp;     /* force EXIT */
            }
          }
          else {
            if((tractive && (tron == 0)) || ((tractive == 0) && tron)) {
               *lp = *spp--;
               pcc = pcb + *lp;
               tractive = 1;
               goto extrap;
            }
          }
          break;
#else
	  fprintf(stdout, "TRAP not enabled in this version.\n");
          dexit();
#endif
      case EXCH:
          tmp1 = *(spp--);
          tmp2 = *spp;
          *spp = tmp1;
          *(++spp) = tmp2; break;
      case TRU:
	  if (NOT *(spp--) == 0) break;
	  if ((*pcc == GET) OR (*pcc == GETV)) pcc++;
	  pcc++;
	  break;
      case FALS:
	  if ( *(spp--) == 0) break;
	  if ((*pcc == GET) OR (*pcc == GETV)) pcc++;
	  pcc++;
	  break;
      case ADV:  ++(*(pcb + *(spp--))); break;
      case ESTOP:
	  fprintf(stdout,"\nVM ESTOP at %u\n",
                 (uint)(pcc - pcb - 1));
          fflush(stdout);
          update_hist();
          pc = pcc;
	  if(recovr(spp)) { pcc = pcb + *lp; break;}
          else { exit(3);}
      case ADIFF:
          tmp3 = *spp--;
#ifdef MULTICS
          *spp -= tmp3; (*spp) &= MASK32; break;
#else
          *spp -= tmp3; break;
#endif
      case SHIFTR:
          tmp3 = *(spp--);
          tmp4 = *spp;
          *spp = tmp4 >> tmp3; break;
      case SHIFTL:
          tmp3 = *(spp--);
          *spp = *spp << tmp3; break;
      case DEBUGF:
          debugst = *(spp--);
          vmdebgf = debugst; break;
      case ILL_0:
      case ILL_46:
      case ILL_59:
      case ILL_60:
      case ILL_61:
      case ILL_62:
      case ILL_63:
      case ILL_64:
      case ILL_65:
      case ILL_66:
      case ILL_67:
      case ILL_68:
      case ILL_69:
      case ILL_70:
      case ILL_71:
      case ILL_72:
      case ILL_73:
      case ILL_74:
      case ILL_75:
      case ILL_76:
      case ILL_77:
      case ILL_78:
      case ILL_79:
	  fprintf(stdout,"** Illegal VM op code %d at address %d **\n",
	    instruct, (uint)(pcc-pcb-1)); pc = pcc;
#ifdef DEBUG_VM
          fprintf(stdout,"inst_cnt: %d\n", inst_cnt);
#endif
          pc = pcc;
          update_hist();
	  if(recovr(spp)){ pcc = pcb + *lp; break;}
          else { exit(3);}
      case EMULATE:
	  fprintf(stdout, "\n EMULATE not implemented.\n");
          dexit();	

      default:
      *lp = pcc - pcb;
      *(++lp) = instruct; pcc = pcb + instruct;
	if (interrupt == 1) {
	  interrupt = 0;
          update_hist();
	  if(recovr(spp)){ pcc = pcb + *lp; break;}
          else { exit(3);}
	}
    break;
    }      /*  end switch  */
  }      /*  end for ( ;; )  */
}

#ifdef MULTICS
sint recovr(spp)
stkwrds *spp;
#else
sint recovr(stkwrds *spp)
#endif
{
    char escc;

    /* shostack(); */
#ifndef MULTICS
    __fpurge(stdin);
#endif
    /* getchar(); */
    fprintf(stdout, "\nDo you want to attempt restart? (Y/N): ");
    fflush(stdout);
    escc = getchar();
#ifndef MULTICS
    __fpurge(stdin);
#endif
    getchar();
    if( tolower( escc) == 'y') {
       spp = ost;
       lp = lst;
       *lp = 32768;
       return(TRUE);
    }
    shostack();
    return(FALSE);
}
