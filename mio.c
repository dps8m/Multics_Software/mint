/*                                                         mio.c   */
/*  file and terminal I/O operations of MINT VM      */

/*  @Copyright 1990  T.W. Griswold and M.D. Godfrey  */
/*  Updated 7 January 2002 to correct handling of files with ^Z
    or cr/lf termination when used on Unix-like systems.

    Updated 20 May 2003 to incorporate use of readline() for
    systems that support it.                                       */

#include "syshdrs.h"
#include "mtext.h"

char prmpt = TRUE;
#ifdef READLINE
char *rc, *rcn;
sint new_line = 0;
#endif

/* Update readline history file on exit */

void update_hist()
{
#ifdef READLINE
  char     hist_file[255];
  char    *hist_ptr;

  hist_ptr = getenv("HOME");
  strcpy(hist_file, hist_ptr);
  strcat(hist_file, "/.mint_hist");
  write_history(hist_file);
#endif
  return;
}

/*  Input char, primitive INCH  */

#ifdef MULTICS
sint inchar(segnum)
sint segnum;
#else
sint inchar(sint segnum)
#endif
{
  FILE *segptr;
#ifndef MULTICS
  char str[4] = "VM>";
#else
  char *str;
#endif
  char c;

#ifdef MULTICS
  str = "VM>";
#endif
  if(start_string) {
     sscanf(start_ptr, "%c", &c);
     start_ptr++;
     if( c == '\n') { c = CR; start_string = FALSE;}
     return((sint)c);
  }
  if (segnum == 0) {
#ifdef READLINE
    if(new_line == 0) {
      rc = readline(str); // printf("rc: %s\n", rc);
      rcn = rc;
      if(*rc != 0)add_history(rc);
      new_line = 1;
    }
    if (*rc == 0) c = '\n';
    else sscanf(rc, "%c", &c);
    while( c == '\x1a') {rc++; sscanf(rc, "%c", &c); }
    while( c == '\r')   {rc++; sscanf(rc, "%c", &c);
        if(*rc == 0) c = '\n';}
    if( c == '\n')   {c = CR; new_line = 0; free(rcn); rcn = NULL;}
    rc++;
#else
    if( prmpt) { printf("%s", str); prmpt = FALSE;}
    c = getchar();
    if( c == '\x1a') c = getchar();
    if( c == '\r') c = getchar();
    if( c == '\n') {c = CR; prmpt = TRUE;}
#endif
  }
  else {
    if (segnum < 1 OR segnum > NSEGS) {
      printf("\n** INCH: illegal segment number: %d **",segnum);
      dexit();
    }
    if (seglist[segnum] != SEQINP) {
      printf("\n** INCH: segment %d not open **",segnum);
      dexit();
    }
    segptr = s_ptr[segnum];
    c = fgetc(segptr);
    if ( c == '\x1a') c = fgetc(segptr);
    if ( c == '\r') c = fgetc(segptr);
    if ( c  == EOF) return((sint) 0x100);
    else {
      if (c == '\n') c = CR;
    }
    return((sint)c);
  }
  return((sint)c);
}

/*  Output char, primitive OPCH  */

#ifdef MULTICS
void outchar(chr, segnum)
sint chr;
sint segnum;
#else
void outchar(sint chr, sint segnum)
#endif
{
  FILE *segptr;

  if (chr == CR) chr = '\n';
  if (segnum == 0) {
     if( chr == '\n') prmpt = TRUE; else prmpt = FALSE;
     fputc(chr, stdout); }
  else {
    if (segnum < 1 OR segnum > NSEGS) {
      printf("\n** OPCH: illegal segment number: %d **",segnum);
      dexit();
    }
    if (seglist[segnum] != SEQOUT) {
      printf("\n** OPCH: segment %d not open **",segnum);
      dexit();
    }
    segptr = s_ptr[segnum];
    fputc(chr,segptr);
  }
}

/*  get available segment for opnf()  */

sint getseg()
{
  sint i;
  for ( i = 1 ; i <= NSEGS ; ++i )
    if (seglist[i] == 0) return(i);
  return(0);
}

/*  Open file, primitive OPENF  */

#ifdef MULTICS
sint opnf(cap, type)
uint cap;
sint type;
#else
sint opnf(uint cap, sint type)
#endif
{
  sint segnum;
  sint t1;
  FILE *fd;
  char filnam[FNAMSIZ+1];

  if (NOT(type == SEQINP OR type == SEQOUT)) {
    printf("\n** OPENF: illegal file type: %d **",type);
    dexit();
  }
  if ( (segnum = getseg() ) == 0) {
    printf("\n** OPENF: No segment available **");
    dexit();
  }
  if ((t1 = Cstr(cap, filnam, FNAMSIZ) ) < 0 ) {
    printf("\n** OPENF: name too long: %s **",filnam);
    dexit();
  }
  if (t1 == 0) {
    printf("\n** OPENF: illegal MINT string **");
    dexit();
  }
/*   Have a segment and a file name. */
/*   Store mode in seglist, open file. */

  seglist[segnum] = type;
  switch(type) {
    case SEQINP:
      while ((fd = fopen(filnam, "r")) == NULL) {
        printf("%s not found.\n", filnam);
/*
          if((fd = fopen(filnam, "w+")) == NULL) {
          printf("\n** OPENF: input file %s cannot be opened **",
                 filnam);
*/
        printf("\nNew filename or CR to exit VM: ");
#ifdef READLINE
        rc = readline(NULL);
        if(*rc != 0) {
          add_history(rc);
          strcpy(filnam, rc);
        }
        else { update_hist(); exit(2);}
#else
	fgets(filnam, FNAMSIZ, stdin);
        if (strlen(filnam) <= 1) { update_hist(); exit(2);}
	else {
          filnam[ strlen(filnam) - 1] = 0;}
#endif
        printf("New name: %s\n", filnam); fflush(stdout);
      }
      s_ptr[segnum] = fd;
      break;
    case SEQOUT:
      while ((fd = fopen(filnam,"w")) == NULL) {
        printf("\n** OPENF: open error, output file %s **",filnam);
        printf("\nfilename or CR to exit VM: ");
        fgets(filnam, FNAMSIZ, stdin);
        if (strlen(filnam) <= 1) exit(2);
        printf("New name: %s\n", filnam); fflush(stdout);
      }
      s_ptr[segnum] = fd;
      break;
    default:
      printf("\n** OPENF:  file %s type error: %d **",filnam,type);
      dexit();
  }
  return(segnum);
}

/*  Close file, primitive CLOSEF  */

#ifdef MULTICS
void closf(segnum)
sint segnum;
#else
void closf(sint segnum)
#endif
{
  sint type;

  if (segnum == 0) return;      /*  console  */
  if (segnum < 0 OR segnum > NSEGS) {
    printf("\n** CLOSEF: bad segment number: %d **\n",segnum);
    dexit();
  }
  if (s_ptr[segnum] == NULL) {
    printf("\n** CLOSEF: NULL file pointer, segment %d **\n",segnum);
    dexit();
  }
  if (NOT((type = seglist[segnum]) == SEQINP OR type == SEQOUT)) {
    printf("\n** CLOSEF: bad segment type: %d\n",type);
    dexit();
  }
  if (fclose(s_ptr[segnum]) == -1) {
    printf("\n** CLOSEF: close error, segment %d **\n",segnum);
    dexit();
  }
  seglist[segnum] = 0;
  s_ptr[segnum] = NULL;
}
