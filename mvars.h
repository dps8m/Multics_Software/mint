/*                                                         mvars.h   */
/*  Mint virtual machine, 32 bit ANSI-C version
    Originally written for MINT-2 by T. W. Griswold, Feb 4 1984
    Converted to MINT-3, ANSI-C and updated by M. D. Godfrey - 1989

    @Copyright 1990  T.W. Griswold and M.D. Godfrey                  */

/* Declarations of GLOBAL variables.     */

#ifdef DEBUG_VM
  uint   trbuf[MAXTRB+2];
  uint   trinst[MAXTRB+2];
  sint    trsp[MAXTRB+2];
  sint    tropstk[MAXTRB+2][2];
  sint    trbptr = 1;
#endif

sint      verbose;
sint      curr_ost_limit;
sint      curr_lst_limit;
stkwrds  *lst;                        /*  link stack */
stkwrds  *ost;                        /*  operand stack */

sint      vmdebgf;                    /*  global debug mode flag */
sint      interrupt;
vmwrds    vmemsize;                   /*  VSTORE size */
stkwrds  *lp;                         /*  link stack pointer */
vmwrds   *pc;
vmwrds   *pcb;                        /*  program counter = pointer to vst */
stkwrds  *sp;                         /*  operand stack pointer */

sint      seglist[NSEGS+1];           /*  I/O segments */
FILE     *s_ptr[NSEGS+1];
sint      start_string;
char     *start_ptr;
char      start_str_addr[MAXARGLEN+3];
